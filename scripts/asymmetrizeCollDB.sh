#!/bin/bash
Help()
{
    echo "This script changes a CollDB file to make settings of selected collimators asymmetric."
    echo
    echo "Prepare an input files of the following format:"
    echo "collimator1 ngap move dir plane"
    echo "collimator2 ngap move dir plane"
    echo "..."
    echo
    echo "where collimatorX is the collimator name (case-sensitive) as writte in the CollDB file,"
    echo "ngap is the nominal gap setting in sigma, move is the amount that one jaw should move out in sigma,"
    echo "dir is 0 for moving out the positive jaw or 1 for the negative jaw, and plane is H, V or S."
    echo
    echo "Run the script as asymmetrizeCollDB.sh arg1 arg2 arg3 arg4 arg5, where" 
    echo "arg1 is the path to a CollDB file, arg2 is the path to a twiss file,"
    echo "arg3 is the path to the text file containing the list of collimators to asymmetrize and their respective settings,"
    echo "arg 4 is the energy in [GeV] and arg5 is the normalized emittance in [µm rad]."
    echo "The final file is called temp.dat."
}

while getopts ":h" option; do
    case $option in
        h) # display Help
            Help
            exit;;
    esac
done            

if [ -z "$1" ]
then
    echo "argument 1 not provided. Run asymmetrizeCollDB.sh -h for help."
    exit;
fi
if [ -z "$2" ]
then
    echo "argument 2 not provided. Run asymmetrizeCollDB.sh -h for help."
    exit;
fi



cp $1 temp.dat

ex=$5
nrj=$4
PMASS=0.938

input=$3
while IFS= read -r line
do
    echo $line
    collimator=$(echo $line | awk '{print $1}')
    nGap0=$(echo $line | awk '{print $2}')
    nOfs0=$(echo $line | awk '{print $3}')
    dir=$(echo $line | awk '{print $4}')
    plane=$(echo $line | awk '{print $5}') 
    echo "collimator: "$collimator
    echo "original gap: "$nGap0" sigma"
    echo "requested offset: "$nOfs0" sigma"
    echo "direction [0/1] for [pos/neg]: "$dir
    
    if [ $plane = H ]
    then
        grep NAME $2 | tail --lines=1 > remove.txt
        if [ ! -s remove.txt ]
        then
            echo "Twiss header not found, exiting..."
            exit 1
        else
            BETXCOL=$(awk -v name='BETX' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
            BETXCOL=$((BETXCOL-1)) 
            grep -iw $collimator $2 | head --lines=1 > remove2.txt
            BETA=$(awk '{print $'"$BETXCOL"'}' remove2.txt)
            echo "BETX is "$BETA" m"
            SIGMA=$(awk 'BEGIN {printf sqrt('"$BETA"'*'"$ex"'*'"$PMASS"'/'"$nrj"')/1000}') 
        fi
    elif [ $plane = V ]
    then
        grep NAME $2 | tail --lines=1 > remove.txt
        if [ ! -s remove.txt ]
        then
            echo "Twiss header not found, exiting..."
            exit 1
        else
            BETYCOL=$(awk -v name='BETY' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
            BETYCOL=$((BETYCOL-1)) 
            grep -iw $collimator $2 | head --lines=1 > remove2.txt
            BETA=$(awk '{print $'"$BETYCOL"'}' remove2.txt)
            echo "BETY is "$BETA" m"
            SIGMA=$(awk 'BEGIN {printf sqrt('"$BETA"'*'"$ex"'*'"$PMASS"'/'"$nrj"')/1000}') 
        fi
    else
        grep NAME $2 | tail --lines=1 > remove.txt
        if [ ! -s remove.txt ]
        then
            echo "Twiss header not found, exiting..."
            exit 1
        else
            BETXCOL=$(awk -v name='BETX' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
            BETXCOL=$((BETXCOL-1)) 
            grep -iw $collimator $2 | head --lines=1 > remove2.txt
            BETX=$(awk '{print $'"$BETXCOL"'}' remove2.txt)
            SIGX=$(awk 'BEGIN {printf sqrt('"$BETX"'*'"$ex"'*'"$PMASS"'/'"$nrj"')/1000}') 
            echo "BETX is "$BETX" m and SIGX is "$SIGX" m"              
            BETYCOL=$(awk -v name='BETY' '{for (i=1;i<=NF;i++) if ($i==name) print i; exit}' remove.txt)
            BETYCOL=$((BETYCOL-1)) 
            grep -iw $collimator $2 | head --lines=1 > remove2.txt
            BETY=$(awk '{print $'"$BETYCOL"'}' remove2.txt)
            SIGY=$(awk 'BEGIN {printf sqrt('"$BETY"'*'"$ex"'*'"$PMASS"'/'"$nrj"')/1000}')            
            echo "BETY is "$BETY" m and SIGY is "$SIGY" m"            
            SIGMA=$(awk 'BEGIN {printf sqrt(0.5*'"$SIGX"'*'"$SIGX"'+0.5*'"$SIGY"'*'"$SIGY"')}')
            echo "sigma is "$SIGMA" m"
        fi 
    fi 

    nOfs1=$(awk 'BEGIN {printf '"$SIGMA"'*'"$nOfs0"'}')
    nOfs2=$(awk 'BEGIN {printf '"$nOfs1"'*0.5}')
    nGap2=$(awk 'BEGIN {printf '"$nGap0"'+'"$nOfs0"*0.5'}')
    echo "sigma is "$SIGMA" m"
    echo "offset of single jaw is "$nOfs1" m"
    echo "Applying the center offset "$nOfs2" m and changing sigma gap to "$nGap2 
    cp temp.dat temp2.dat
    
    if [ $dir = 0 ]
    then
        echo "positive offset..."
        awk 'BEGIN{FS=OFS=" "}/'$collimator'*/{$2='"$nGap2"'; $6='"$nOfs2"'}1' temp2.dat > temp.dat
    else
        echo "negative offset..."
        awk 'BEGIN{FS=OFS=" "}/'$collimator'*/{$2='"$nGap2"'; $6=-'"$nOfs2"'}1' temp2.dat > temp.dat
    fi
    echo ""
    echo ""
done < "$input"

rm temp2.dat remove.txt remove2.txt
